const async = require('async');
const MongoClient = require('mongodb').MongoClient;
const read = require('./utils/read');

const boards = [
  // TODO finish here
];

exports.handler = (event, context, exit) => {
  async.waterfall([

    (next) => { //read local mongo_settings, not present in deployed function
      read('file', 'mongo_settings.json', (error, file) => {
        if (error) {
          return next(null);
        }
        next(null, JSON.parse(file));
      });
    },

    (mongoSettings, next) => { //if no local mongo_settings, fetch them from S3
      if (!mongoSettings) {
        return next('Implement mongo_settings.json fetch from S3.'); //TODO
      }
      next(null, mongoSettings.mongoUrl);
    },

    (mongoUrl, next) => {
      MongoClient.connect(mongoUrl, {}, (err, db) => {
        if (err) {
          return next(err);
        }
        next(null, db);
      });
    },

    (db, next) => {

    }

  ], exit);
};
