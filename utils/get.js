const http = require('http');

const get = (url, callback) => {
  var request = http.get(url, function(response) {
    if (response.statusCode !== 200) {
      throw 'GET ' + url ' received error in the response';
    }
    response.setEncoding('utf8');
    var body = '';
    response.on('data', function (chunck) {
      body += chunck;
    });
    response.on('end', function () {
      var object = JSON.parse(body);
      callback(null, object);
    });
  });
  request.on('error', function(error) {
    callback(error, null);
  });
};

module.exports = get;
