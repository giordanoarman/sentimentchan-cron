const fs = require('fs');

const read = (type, url, callback) => {

  fs.open(url, 'r', (error, fd) => {
    if (error) {
      return callback(error);
    }

    fs.fstat(fd, (err, stats) => {
      if (err) {
        return callback(err);
      }
      if (type === 'file' && stats.isDirectory()) {
        return callback('Path is directory: ' + url);
      }
      if (type === 'directory' && stats.isFile()) {
        return callback('Path is file: ' + url);
      }

      var bufferSize = stats.size;
      var chunkSize = 512;
      var buffer = new Buffer(bufferSize);
      var bytesRead = 0;
      while (bytesRead < bufferSize) {
        if ((bytesRead + chunkSize) > bufferSize) {
          chunkSize = bufferSize - bytesRead;
        }
        fs.read(fd, buffer, bytesRead, chunkSize, bytesRead);
        bytesRead += chunkSize;
      }
      fs.close(fd);

      var file = buffer.toString('utf8', 0, bufferSize);
      callback(null, file);
    });
  });

};

module.exports = read;
